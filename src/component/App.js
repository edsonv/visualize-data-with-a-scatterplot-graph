import React from 'react';

import ScatterplotGraph from './ScatterplotGraph';

import './styles/App.scss';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      dataset: undefined,
      error: null
    };
  }

  fetchData = async () => {
    try {
      this.setState({ loading: true, error: null });
      const response = await fetch(
        'https://raw.githubusercontent.com/freeCodeCamp/ProjectReferenceData/master/cyclist-data.json'
      );
      const dataset = await response.json();
      this.setState({ loading: false, dataset });
    } catch (error) {
      this.setState({ loading: false, error });
    }
  };

  componentDidMount() {
    this.fetchData();
  }

  render() {
    if (this.state.loading && !this.state.dataset) {
      return <p>Loading...</p>;
    }
    // console.log(this.state.dataset);
    return (
      <React.Fragment>
        <ScatterplotGraph dataset={this.state.dataset} />
      </React.Fragment>
    );
  }
}

export default App;
