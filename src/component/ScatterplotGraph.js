import React from 'react';
import * as d3 from 'd3';

import './styles/ScatterplotGraph.scss';

class ScatterplotGraph extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      width: 854,
      height: 480,
      padding: 60
    };
  }
  componentDidMount() {
    this.drawChart();
  }

  drawChart = async () => {
    // Insert svg element
    const svg = d3
      .select('.svgCanvas')
      .append('svg')
      .classed('svgCanvas--svg', true)
      .attr('viewBox', '0 0 854 480')
      // .attr('width', this.state.width)
      // .attr('height', this.state.height)
      .attr('preserveAspectRatio', 'xMinYMin meet');

    // Insert title
    svg
      .append('text')
      .attr('id', 'title')
      .text('Dopaje en carreras profesionales de ciclismo')
      .attr('x', 400)
      .attr('y', 20)
      .attr('text-anchor', 'middle');

    // Subtitle
    svg
      .append('text')
      .attr('id', 'subtitle')
      .text("35 mejores tiempos en Alpe d'Huez")
      .attr('x', 400)
      .attr('y', 48)
      .attr('text-anchor', 'middle');

    // Arrays of years and times
    let years = this.props.dataset.map(el => el.Year);
    let times = this.props.dataset.map(el => d3.timeParse('%M:%S')(el.Time));
    console.log(years);

    // Define the scales
    let x = d3
      .scaleLinear()
      .domain([d3.min(years) - 1, d3.max(years) + 1])
      .range([0, this.state.width - this.state.padding * 2]);

    let y = d3
      .scaleTime()
      .domain(d3.extent(times))
      .range([0, this.state.height - this.state.padding * 2]);

    // Define the axes
    let xAxis = d3
      .axisBottom(x)
      // .scale(x)
      .tickFormat(d3.format('d'));

    let yAxis = d3
      .axisLeft(y)
      // .scale(y)
      .tickFormat(d => d3.timeFormat('%M:%S')(d));

    // Draw the x axis
    svg
      .append('g')
      .attr('id', 'x-axis')
      .attr(
        'transform',
        `translate(${this.state.padding}, ${this.state.height -
          this.state.padding +
          20})`
      )
      .call(xAxis);

    // Draw the y axis
    svg
      .append('g')
      .attr('id', 'y-axis')
      .attr(
        'transform',
        `translate(${this.state.padding}, ${this.state.padding + 20})`
      )
      .call(yAxis);

    let color = d3.scaleOrdinal(d3.schemeCategory10);

    // Append tooltip
    let tooltip = d3
      .select('.svgCanvas')
      .append('div')
      .attr('id', 'tooltip')
      .style('opacity', 0);

    // Plot the document
    svg
      .selectAll('circle')
      .data(this.props.dataset)
      .enter()
      .append('circle')
      .attr('class', 'dot')
      .attr('r', 5)
      .attr('cx', (d, i) => x(d.Year) + this.state.padding)
      .attr('cy', (d, i) => y(times[i]) + this.state.padding + 20)
      .attr('data-xvalue', (d, i) => d.Year)
      .attr('data-yvalue', (d, i) => times[i].toLocaleString())
      .style('fill', d => color(d.Doping !== ''))
      .on('mouseover', (d, i) => {
        tooltip
          .transition()
          .duration(200)
          .style('opacity', 0.9);

        tooltip
          .html(
            `${d.Name} (${d.Nationality})<br>Year: ${d.Year}, Time: ${d.Time}<br><br> ${d.Doping}`
          )
          .attr('data-year', d.Year)
          .style('left', `${d3.event.pageX}px`)
          .style('top', `${d3.event.pageY}px`);
      })
      .on('mouseout', (d, i) => {
        tooltip
          .transition()
          .duration(200)
          .style('opacity', 0);
      });

    // Define the legend
    let legend = svg
      .selectAll('.legend')
      .data(color.domain())
      .enter()
      .append('g')
      .attr('class', 'legend')
      .attr('id', 'legend')
      .attr(
        'transform',
        (d, i) => `translate(-60, ${this.state.height / 2 - i * 20})`
      );

    legend
      .append('rect')
      .attr('x', this.state.width)
      .attr('width', 10)
      .attr('height', 10)
      .style('fill', color);

    legend
      .append('text')
      .attr('x', this.state.width - 2)
      .attr('y', 9)
      .style('text-anchor', 'end')
      .text(d =>
        d ? 'Riders with doping allegations' : 'No doping allegations'
      );
  };
  render() {
    return <div className='svgCanvas'></div>;
  }
}

export default ScatterplotGraph;
